const int resistorPin = A1;
const int triggerPin = 9;

const int threshold = 20;
const int loopDelay = 100;
const int triggerDelay = 500;

int previousValue = 5000;
int activated = 0;

void setup(){
  Serial.begin(9600);
  pinMode(resistorPin, INPUT);
  pinMode(triggerPin, OUTPUT);
  digitalWrite(triggerPin, HIGH);
}

void loop(){
  int value = analogRead(resistorPin);
  Serial.print("Value: ");
  Serial.println(value);
  if (!activated && value > previousValue + threshold) {
    Serial.println("\nCountry roads, take me gnome...\n");
    activated = 1;
    digitalWrite(triggerPin, LOW);
    delay(triggerDelay);
    digitalWrite(triggerPin, HIGH);
  } else if (activated && value < previousValue - threshold) {
    activated = 0;
  }
  previousValue = value;
  delay(loopDelay);
}
